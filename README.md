# misskey-gruvbox

This is a collection of themes for [Misskey](https://github.com/misskey-dev/misskey) inspired by vim [gruvbox](https://github.com/morhetz/gruvbox) themes.

## Frontend installation

If you misskey user you can install theme in Settings:

1. Choose theme and copy information from file.
2. Go to Settings -> Themes.
3. Click Install a theme.
4. Paste to Theme code field what you copied.
5. (Optional) Click Preview.
6. Click Install.
7. Now you can choose theme from Settings -> Themes menu.

## Backend installation

If you misskey admin you can add themes for all users:

1. Copy themes to `misskey/packages/client/src/themes/`.
2. Add theme names to list inside `misskey/packages/client/src/scripts/theme.ts`.

Looks like this:

```typescript
export const getBuiltinThemes = () => Promise.all(
	[
    'l-light',
    'l-coffee',
    'l-apricot',
    'l-rainy',
    'l-vivid',
    'l-cherry',
    'l-sushi',
    'l-u0',
    'misskey-gruvbox-light-hard',
    'misskey-gruvbox-light-medium',
    'misskey-gruvbox-light-soft',

    'd-dark',
    'd-persimmon',
    'd-astro',
    'd-future',
    'd-botanical',
    'd-green-lime',
    'd-green-orange',
    'd-cherry',
    'd-ice',
    'd-u0',
    'misskey-gruvbox-dark-hard',
    'misskey-gruvbox-dark-medium',
    'misskey-gruvbox-dark-soft',
	].map(name => import(`../themes/${name}.json5`).then(({ default: _default }): Theme => _default)),
);
```

3. (Optional) Inside `misskey/packages/client/src/store.ts` you can choose default themes for instance.

```typescript
import lightTheme from '@/themes/misskey-gruvbox-light-soft.json5';
import darkTheme from '@/themes/misskey-gruvbox-dark-medium.json5';
```

4. Rebuild misskey.

  * `git checkout master`
  * `git pull`
  * `git submodule update --init`
  * `yarn install`
  * `NODE_ENV=production yarn build`
  * `yarn migrate`

5. Restart misskey.

  * `sudo systemctl restart misskey`

6. Now you can choose theme from Settings -> Themes menu.
